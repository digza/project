DATABASE = {
    u'drivername': u'postgres',
    u'host': u'maxime.cpyzhnigbcgb.eu-west-1.rds.amazonaws.com',
    u'port': u'5432',
    u'username': u'maxime',
    u'password': u'maximesetup',
    u'database': u'maxime'
}

from sqlalchemy.engine.url import URL

from sqlalchemy import create_engine
engine = create_engine(URL(**DATABASE), pool_size=0, max_overflow=-1)

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from sqlalchemy import Column, Integer, String
class Movie(Base):
    __tablename__ = 'Movie'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    length = Column(Integer)
    synopsis = Column(String)

    def __repr__(self):
        return "<Movie(name=' %s', length=' %s', synopsis=' %s')>" % \
            self.name, self.length, self.synopsis

Base.metadata.create_all(engine)

from sqlalchemy.orm import sessionmaker
session = sessionmaker(bind=engine)
session = session()

def add_movie(movie, title, length, syn):
#adds a movie to table
    movie.name = title
    movie.length = str(length)
    movie.synopsis = syn
    session.add(movie)
    session.commit()

def del_movie(title, number = 1):
#deletes a movie from table
    try:
        for movie in session.query(Movie):
            if movie.name == title:
                session.delete(movie)
                session.commit()
                number -= 1
                if number <= 0:
                    break
    except Exception:
        raise Exception('No movie ' + title + '  was found, deletion aborted')
        
def update_movie(title, new_title=None, new_len = -1, new_syn = None):
#updates information about a movie
    try:
        for movie in session.query(Movie):
            if movie.name == title:
                if new_title:
                    movie.name = new_title
                if new_len != - 1:
                    movie.length = str(new_len)
                if new_syn:
                    movie.synopsis = new_syn
                session.commit()
                break
    except Exception:
        raise Exception('No movie ' + title + ' was found, update aborted')

from movie_class import *
import unittest

class MovieClassTest(unittest.TestCase):
    
    def setUp(self):
        session.query(Movie).delete()
        session.commit()

    def test_add1(self):
        #checks if movie is added (checking number of registered movies)
        for i in range(1,11):
            add_movie(Movie(), "foo", 42, "bar")
            self.assertEqual(i, session.query(Movie).count())
            
    def test_add2(self):
        #checks if movie information have been correctly registered
        add_movie(Movie(), "foo", 42, "bar")
        for movie in session.query(Movie):
            self.assertEqual("foo", movie.name)
            self.assertEqual(42, movie.length)
            self.assertEqual("bar", movie.synopsis)
    
    def test_update_simple(self):
        #checks if the updated information have been registered
        add_movie(Movie(), "foo", 42, "bar")
        update_movie("foo", "riri", 0, "fifiloulou")
        for movie in session.query(Movie):
            self.assertEqual("riri", movie.name)
            self.assertEqual(0, movie.length)
            self.assertEqual("fifiloulou", movie.synopsis)

    def test_update_long(self):
        #same test as before but for several movies
        for i in range(1,11):
            add_movie(Movie(), str(i), i, str(-i))
        for i in range(1,11):
            update_movie(str(i), "the answer to", 42, "life is")
        for movie in session.query(Movie):
            self.assertEqual("the answer to", movie.name)
            self.assertEqual(42, movie.length)
            self.assertEqual("life is", movie.synopsis)

    def test_update_name_only(self):
        #checks if only name is updated
        add_movie(Movie(), "foo", 42, "bar")
        update_movie("foo", new_title = "riri")
        for movie in session.query(Movie):
            self.assertEqual(movie.name, "riri")
            self.assertEqual(movie.length, 42)
            self.assertEqual(movie.synopsis, "bar")

    def test_update_len_only(self):
        #checks if only length is updated
        add_movie(Movie(), "foo", 42, "bar")
        update_movie("foo", new_len = 43)
        for movie in session.query(Movie):
            self.assertEqual(movie.length, 43)
            self.assertEqual(movie.synopsis, "bar")
            self.assertEqual(movie.name, "foo")

    def test_update_syn_only(self):
        #checks if only synopsis is updated
        add_movie(Movie(), "foo", 42, "bar")
        update_movie("foo", new_syn = "foofoo")
        for movie in session.query(Movie):
            self.assertEqual(movie.synopsis, "foofoo")
            self.assertEqual(movie.length, 42)
            self.assertEqual(movie.name, "foo")

    def test_update_error(self):
        #checks if update is not done (expected behaviour)
        add_movie(Movie(), "foo", 42, "bar")
        update_movie("error", "riri", 0, "fifiloulou")
        for movie in session.query(Movie):
            self.assertEqual("foo", movie.name)
            self.assertEqual(42, movie.length)
            self.assertEqual("bar", movie.synopsis)

    def test_update_error_except(self):
        #checks if exception is raised (expected behaviour)
        add_movie(Movie(), "foo", 42, "bar")
        update_movie("error", "riri", 0, "fifiloulou")
        self.assertRaises(Exception, lambda: update_movie, "error", "riri", 0, "fifiloulou")

    def test_del_simple(self):
        #checks if movie deletion happened
        add_movie(Movie(), "foo", 42, "bar")
        del_movie("foo")
        self.assertEqual(0, session.query(Movie).count())

    def test_del_long_minus_1(self):
        #same test as before but for several movies
        for i in range(1,11):
            add_movie(Movie(), str(i), i, str(-i))
        del_movie(str(i))
        self.assertEqual(0, session.query(Movie).filter(Movie.name == str(i)).count())
        self.assertEqual(i - 1, session.query(Movie).count())

    def test_del_long_del_all(self):
        #same test as before, testing if all movies are deleted (expected behaviour)
        for i in range(1,11):
            add_movie(Movie(), "lol", i, str(-i))
        del_movie("lol", session.query(Movie).count())
        self.assertEqual(0, session.query(Movie).count())

    def test_del_duplicate_minus_one(self):
        #checks if only one movie with corresponding name is deleted
        for i in range(1,11):
            add_movie(Movie(), "lol", i, str(-i))
        del_movie("lol")
        self.assertEqual(i - 1, session.query(Movie).count())

    def test_del_error(self):
        #checks if deletion aborted (expected behaviour)
        add_movie(Movie(), "lol", 42, "lal")
        del_movie("42")
        self.assertEqual(1, session.query(Movie).count())

    def test_del_error_except(self):
        #checks if exception is raised (expected behaviour)
        add_movie(Movie(), "foo", 42, "bar")
        del_movie("error")
        self.assertRaises(Exception, lambda: update_movie, "error")

if __name__ == '__main__':
    unittest.main(verbosity=2)
